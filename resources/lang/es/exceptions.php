<?php

return [
    'unknown' => "Se ha producido un error desconocido. Por favor, póngase en contacto con el servicio técnico.",
    'invalid_wager' => "Apuesta no válida.",
    'no_plays_selected' => "No se han seleccionado juagadas.",
    'no_parameters_loaded_yet' => "Parámetros no recuperados.",
    'min_lines' => "Debe seleccionar al menos :min líneas.",
    'max_lines' => "No puede seleccionar más de :max líneas.",
    'min_wager' => "La apuesta mínima es: :min.",
    'max_wager_straight' => "La apuesta máxima por derecho es: :max.",
    'max_wager_parlay' => "La apuesta máxima parley es: :max.",
    'max_payoff' => "El premio máximo es: :max.",
    'max_odds' => "El factor máximo es: :max.",
    'invalid_combination' => "Combinación iválida.",
];