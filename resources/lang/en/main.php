<?php

return [
    'bet_types' => [
        '1' => 'Moneyline',
        '2' => 'Runline',
        '3' => 'Over/Under',
        '4' => 'Super RL',
        '5' => 'Alt RL',
        '6' => 'Score 1st',
        '7' => '1st inning',
        '8' => 'HRE',
    ],

    'periods' => [
        '1' => '',
        '2' => '1H',
        '3' => '2H',
        '4' => '1P',
        '5' => '2P',
        '6' => '3P',
        '7' => '1Q',
        '8' => '2Q',
        '9' => '3Q',
        '10' => '4Q',
        '11' => '',
        '12' => 'S1',
        '13' => 'S2',
        '14' => 'S3',
        '15' => 'S4',
        '16' => 'S5',
        '17' => '',
    ],

    'participants' => [
        'draw'      => "Draw",
        'over'      => "O",
        'under'     => "U",
        'yes'       => "Yes",
        'no'        => "No",
        '1'         => "Draw",
        '2'         => "O",
        '3'         => "U",
        '4'         => "Yes",
        '5'         => "No",
    ],
];