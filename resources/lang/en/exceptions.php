<?php

return [
    'unknown' => "An unknown error occurred. Please contact technical service.",
    'invalid_wager' => "Invalid wager.",
    'no_plays_selected' => "No plays were selected.",
    'no_parameters_loaded_yet' => "Parameters couldn't be retrieved.",
    'min_lines' => "You must select at least :min lines.",
    'max_lines' => "You can't select more than :max lines.",
    'min_wager' => "Minimum wager is: :min.",
    'max_wager_straight' => "Maximum straight wager is: :max.",
    'max_wager_parlay' => "Maximum parlay wager is: :max.",
    'max_payoff' => "Maximum payoff is: :max.",
    'max_odds' => "Maximum odds is: :max.",
    'invalid_combination' => "Invalid combination.",
];