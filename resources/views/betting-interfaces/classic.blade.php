<table class="betting-table">
    <tbody>
        <?php $dist = [15, 85] ?>

        @foreach ($terminal->leagues as $league)

            <tr class="betting-table-league" id="sport-{{$league->getSportId()}}">
                <th colspan="{{ $terminal->maxBTP + 1}}">{{ $league->getName() }}</th>
            </tr>

            <?php $zebra = false ?>

            @foreach ($league->getEvents() as $event)

                <tr class="betting-table-options">
                    <td width="{{ head($dist) }}%">
                        {{ date('g:i A', strtotime($event->getStartTime())) }} [{{ $event->getId() }}]
                    </td>

                    <?php $counter = 0 ?>

                    @foreach ($event->getPeriods() as $period)
                        @foreach ($period->getBetTypes() as $bet_type)
                            <td width="{{ floor(last($dist) / $terminal->maxBTP) }}%" @if($counter % 2 === 0) class="betting-table-even-option" @endif>
                                {{ $bet_type->getName() }} {{ $period->getName()}}
                                @if ($bet_type->getInfo())
                                    ({{ $bet_type->getInfo() }})
                                @endif
                            </td>

                            <?php $counter++ ?>
                        @endforeach
                    @endforeach

                    @for($i = $counter; $i < $terminal->maxBTP; $i++)
                        <td width="{{ floor(last($dist) / $terminal->maxBTP) }}%" @if($i % 2 === 0) class="betting-table-even-option" @endif></td>
                    @endfor

                </tr>

                <?php $class = ['betting-table-lines']; if ($zebra) $class[] = 'betting-table-lines-zebra'; ?>

                <tr class="{{ implode(' ', $class) }}">
                    <td>
                        @foreach($event->getParticipants() as $participant)
                            <span class="betting-table-teams">
                                <img src="{{ team_img($participant->getId().'.png') }}" alt="{{ $participant->getName() }}" onerror="this.src = '{{ team_img('0.png') }}'">
                                <strong>{{ $participant->getRot() }}</strong> {{ $participant->getName() }}
                            </span>
                        @endforeach
                    </td>

                    <?php $counter = 0 ?>

                    @foreach ($event->getPeriods() as $period)
                        @foreach ($period->getBetTypes() as $bet_type)
                            <td @if($counter % 2 === 0) class="betting-table-even-column" @endif>
                                @foreach ($bet_type->getLines() as $line)
                                    @if ($line->getOdds())
                                        <label for="line-{{ $line->getId() }}" class="betting-table-odds">
                                            <input type="checkbox" name="lines[]"
                                                   value="{{ $line->getId().'|'.$line->getSpread().'|'.$line->getOdds() }}"
                                                   id="line-{{ $line->getId() }}"
                                                   data-sport-id="{{ $league->getSportId() }}"
                                                   data-event-id="{{ $event->getId() }}"
                                                   data-period-id="{{ $period->getId() }}"
                                                   data-bet-type-id="{{ $bet_type->getId() }}">
                                            @if (in_array($bet_type->getId(), [2,4,5]))
                                                {{ $line->getSpread() ?: 'PK' }}
                                            @elseif (in_array($line->getParticipant(), [2,3]))
                                                {{ trans('parley-api::main.participants.' . $line->getParticipant()) }}
                                            @endif
                                            {{ $line->getOdds() }}
                                        </label>
                                    @endif
                                @endforeach
                            </td>

                            <?php $counter++ ?>
                        @endforeach
                    @endforeach

                    @for($i = $counter; $i < $terminal->maxBTP; $i++)
                        <td @if($i % 2 === 0) class="betting-table-even-column" @endif></td>
                    @endfor
                </tr>

                <?php $zebra = !$zebra ?>

            @endforeach

        @endforeach
    </tbody>
</table>