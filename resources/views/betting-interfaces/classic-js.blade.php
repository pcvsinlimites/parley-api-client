<script>
    //Checked items
    var checked = [];

    //Select a line
    //Disable sibling lines
    //Update selected lines count
    $(':checkbox[name="lines[]"]').bind('change', function() {
        var $this = $(this);

        //Allow to check
        $this.prop('checked', $this.is(':checked') && checkCombinations($this));
        var isChecked = $this.is(':checked');

        //Disable siblings
        $this.parent().siblings().find(':checkbox').prop('disabled', isChecked);

        //Mark checked
        if (isChecked) {
            checked.push($this);
        }
        //Remove from checked
        else {
            checked = $.grep(checked, function(elem) {
                return elem.val() != $this.val();
            });
        }

        //Update plays count
        plays.text(checked.length);

        //Update odds / payoff
        wager.trigger('betting:update');

        //Update ticket preview
        updateTicket();
    });

    //Click on clear
    //Call "betting:clear" event
    clear.bind('click', function(e) {
        e.preventDefault();
        $(document).trigger('betting:clear');
    });

    //Save current plays
    save.bind('click', function(e) {
        e.preventDefault();

        //Check parameters has been loaded
        if (typeof terminal.loaded !== 'undefined') {
            message("@lang('parley-api::exceptions.no_parameters_loaded_yet')");
            return;
        }

        var $wager = validWager(wager.val());
        var $plays = parseInt(plays.text());
        var $payoff = parseFloat(payoff.val());
        if ($plays <= 0) {
            message("@lang('parley-api::exceptions.no_plays_selected')");
        }
        else if ($wager <= 0) {
            message("@lang('parley-api::exceptions.invalid_wager')");
        }
        else if ($plays < (terminal.custom ? terminal.custom.min_lines : terminal.agency.min_lines)) {
            message("@lang('parley-api::exceptions.min_lines', ['min' => $user->terminal->custom ? $user->terminal->custom->min_lines : $user->terminal->agency->min_lines])");
        }
        else if ($plays > (terminal.custom ? terminal.custom.max_lines : terminal.agency.max_lines)) {
            message("@lang('parley-api::exceptions.max_lines', ['max' => $user->terminal->custom ? $user->terminal->custom->max_lines : $user->terminal->agency->max_lines])");
        }
        else if ($wager < parseFloat(terminal.agency.limits.min_wager)) {
            message("@lang('parley-api::exceptions.min_wager', ['min' => $user->terminal->agency->limits->min_wager])");
        }
        else if ($plays === 1 && $wager > parseFloat(terminal.agency.limits.straight_wager)) {
            message("@lang('parley-api::exceptions.max_wager_straight', ['max' => $user->terminal->agency->limits->straight_wager])");
        }
        else if ($plays > 1 && $wager > parseFloat(terminal.agency.limits.parlay_wager)) {
            message("@lang('parley-api::exceptions.max_wager_parlay', ['max' => $user->terminal->agency->limits->parlay_wager])");
        }
        else if ($payoff > parseFloat(terminal.agency.limits.payoff)) {
            message("@lang('parley-api::exceptions.max_payoff', ['max' => $user->terminal->agency->limits->payoff])");
        }
        else if ((terminal.custom || terminal.agency.odds) && $payoff/$wager > (terminal.custom ? terminal.custom.odds : terminal.agency.odds)) {
            message("@lang('parley-api::exceptions.max_odds', ['max' => $user->terminal->custom ? $user->terminal->custom->odds : $user->terminal->agency->odds])");
        }
        else {

            showConfirmation();

            confirm.bind('click', function(){

                $(this).addClass("disabled");

                showLoading();

                $.post('{{ url(config('parley-api.store_ticket_url')) }}', {
                    "_token" : "{{ csrf_token() }}",
                    "wager" : $wager,
                    "lines[]": checked.map(function(elem) {
                        return elem.val();
                    })
                })
                .done(function() {
                    hideLoading();
                    message("@lang('parley-api::messages.ticket_stored')");
                    $(document).trigger('betting:clear');
                })
                .fail(function() {
                    hideLoading();
                    message("@lang('parley-api::exceptions.unknown')");
                });
            });
        }
    });

    //Clear selected lines - event
    $(document).on('betting:clear', function() {
        while (checked.length) {
            checked.pop().prop('checked', false).trigger('change');
        }
        plays.text(0);
        wager.val('').trigger('change');
    });

    //Update payoff
    wager.bind('change', function() {
        $(this).trigger('betting:update');
        var value = validWager(this.value);
        if (value) save.focus();
    });

    //Update odds / payoff
    wager.on('betting:update', function() {
        var value = validWager(this.value);
        var reward = Math.round(calculateOdds() * parseFloat(value));
        if(payoff.is('input')){
            payoff.val(reward);
        } else {
            payoff.text(reward);
        }
    });

    //Calculate total odds
    function calculateOdds() {
        var total = 1;
        $(checked).each(function(k, elem) {
            var line = elem.val().split('|');
            var odds = parseInt(line[2]);
            total *= 1 + (odds > 0 ? odds/100 : -100/odds);
        });
        return total;
    }

    //Check combinations
    function checkCombinations(input) {
        if (typeof combinations.loaded !== 'undefined') {
            message("@lang('parley-api::exceptions.no_parameters_loaded_yet')");
            return;
        }

        var event = input.data('event-id');
        var sport = input.data('sport-id');
        var period = input.data('period-id');
        var bet_type = input.data('bet-type-id');
        var possible = typeof combinations[sport] !== 'undefined' &&
        typeof combinations[sport][period] !== 'undefined' &&
        typeof combinations[sport][period][bet_type] !== 'undefined' ?
                combinations[sport][period][bet_type] : {};
        var valid = true;

        $(checked).each(function(k, elem) {
            if (elem.data('event-id') == event) {
                var period = elem.data('period-id');
                valid = typeof possible[period] !== 'undefined' && possible[period].indexOf(elem.data('bet-type-id')) >= 0;
            }
        });

        if (valid === false) {
            message("@lang('parley-api::exceptions.invalid_combination')");
        }

        return valid;
    }

    //Retrieve clean wager (invalid wager returns 0)
    function validWager(wager) {
        var value = wager.replace(/[^0-9\-]+/, '').replace(/\-{2,}/, '-');
        return value ? value : 0;
    }

</script>