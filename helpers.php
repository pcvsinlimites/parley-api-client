<?php

/**
 * API base uri
 *
 * @param string $path
 * @return string
 */
function api_base_uri($path = '')
{
    return env('PARLEY_API_URL', 'http://parley-api/').$path;
}

/**
 * API call
 *
 * @param $method
 * @param $uri
 * @param array $data
 * @param array $headers
 * @return \GuzzleHttp\Psr7\Stream
 * @deprecated
 */
function api($method, $uri, $data = [], $headers = [])
{
    if (!array_key_exists('Accept', $headers)) {
        $headers['Accept'] = "application/json";
    }

    return app('guzzle')->request($method, $uri, [
        ($method == 'GET' ? 'query' : 'form_params') => $data,
        'decode_content' => 'gzip',
    ], $headers)->getBody();
}

/**
 * API GET call
 *
 * @param $uri
 * @param array $data
 * @return \GuzzleHttp\Psr7\Stream
 * @deprecated
 */
function api_get($uri, $data = [])
{
    return api('GET', $uri, $data);
}

/**
 * API POST call
 *
 * @param $uri
 * @param array $data
 * @return \GuzzleHttp\Psr7\Stream
 * @deprecated
 */
function api_post($uri, $data = [])
{
    return api('POST', $uri, $data);
}

/**
 * STATIC base uri
 *
 * @param string $path
 * @return string
 */
function static_base_uri($path = '')
{
    return env('PARLEY_STATIC_URL', 'http://parley-static/').$path;
}

/**
 * Static image
 *
 * @param $collection
 * @param $id
 * @param string $size
 * @return string
 */
function parley_img($collection, $id, $size = '24')
{
    return static_base_uri('images/'.$collection.'/'.$size.'/'.$id);
}

/**
 * Team image
 *
 * @param $id
 * @param string $size
 * @return string
 */
function team_img($id, $size = '24')
{
    return parley_img('teams', $id, $size);
}

/**
 * League image
 *
 * @param $league
 * @param string $size
 * @return string
 */
function league_img($league, $size = '32')
{
    return parley_img('leagues', $league, $size);
}

/**
 * Language image
 *
 * @param $lang
 * @param string $size
 * @return string
 */
function lang_img($lang, $size = '24')
{
    return parley_img('lang', $lang, $size);
}

/**
 * Flag image
 *
 * @param $flag
 * @param string $size
 * @return string
 */
function flag_img($flag, $size = '24')
{
    return parley_img('flags', $flag, $size);
}
