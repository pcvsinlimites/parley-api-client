<?php

return [
    'models' => [
        'user' => App\User::class,
    ],

    'store_ticket_url' => url('betting/store'),
];