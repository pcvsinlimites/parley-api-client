<?php

namespace Parley\Api\Models;

/**
 * Class BetType
 * @package Parley\Api\Models
 */
class BetType extends Model
{
    protected $id;
    protected $name;
    protected $lines;
    protected $info;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * @param $line
     */
    public function appendLine($line)
    {
        if (is_null($this->lines)) {
            $this->lines = collect();
        }
        $this->lines->push($line);
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }
}