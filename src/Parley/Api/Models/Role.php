<?php

namespace Parley\Api\Models;

/**
 * Class Role
 * @package Parley\Api\Models
 */
class Role
{
    public static $TERMINAL = 17;
    public static $WEB = 35;
}