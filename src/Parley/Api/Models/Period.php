<?php

namespace Parley\Api\Models;

/**
 * Class Period
 * @package Parley\Api\Models
 */
class Period extends Model
{
    protected $id;
    protected $name;
    protected $bet_types;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getBetTypes()
    {
        return $this->bet_types;
    }

    /**
     * @param $bet_type
     */
    public function appendBetType($bet_type)
    {
        if (is_null($this->bet_types)) {
            $this->bet_types = collect();
        }
        $this->bet_types->push($bet_type);
    }
}