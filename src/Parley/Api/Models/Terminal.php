<?php

namespace Parley\Api\Models;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Terminal
 * @package Parley\Api\Models
 */
class Terminal extends Model implements Arrayable
{
    public $id;
    public $agency_id;
    public $ticket_expiration_time;
    public $self_service;
    /**
     * @var CustomTerminal
     */
    public $custom;
    /**
     * @var Agency
     */
    public $agency;

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}