<?php

namespace Parley\Api\Models;

/**
 * Class League
 * @package Parley\Api\Models
 */
class League extends Model
{
    protected $id;
    protected $sport_id;
    protected $name;
    protected $events;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @return mixed
     */
    public function getSportId()
    {
        return $this->sport_id;
    }
}