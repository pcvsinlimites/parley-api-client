<?php

namespace Parley\Api\Models;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class CustomTerminal
 * @package Parley\Api\Models
 */
class CustomTerminal extends Model implements Arrayable
{
    protected $terminal_id;
    protected $min_lines;
    protected $max_lines;
    protected $favorites;
    protected $underdogs;
    protected $odds;

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        // TODO: Implement toArray() method.
    }
}