<?php

namespace Parley\Api\Models;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class AgencyLimits
 * @package Parley\Api\Models
 */
class AgencyLimits extends Model implements Arrayable
{
    public $agency_id;
    public $currency_id;
    public $min_wager;
    public $straight_wager;
    public $parlay_wager;
    public $payoff;
    public $sales;
    public $straight_sales;
    public $combination_sales;

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}