<?php

namespace Parley\Api\Models;

/**
 * Class Participant
 * @package Parley\Api\Models
 */
class Participant extends Model
{
    protected $id;
    protected $rot;
    protected $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getRot()
    {
        return $this->rot;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}