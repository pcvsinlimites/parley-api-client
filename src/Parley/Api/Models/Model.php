<?php

namespace Parley\Api\Models;

/**
 * Class Model
 * @package Parley\Api\Models
 */
class Model
{
    /**
     * Model constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->$key = $value;
        }
    }
}