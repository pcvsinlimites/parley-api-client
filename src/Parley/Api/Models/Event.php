<?php

namespace Parley\Api\Models;

/**
 * Class Event
 * @package Parley\Api\Models
 */
class Event extends Model
{
    protected $id;
    protected $startTime;
    protected $participants;
    protected $periods;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return mixed
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @return mixed
     */
    public function getPeriods()
    {
        return $this->periods;
    }
}