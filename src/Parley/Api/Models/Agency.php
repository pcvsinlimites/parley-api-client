<?php

namespace Parley\Api\Models;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Agency
 * @package Parley\Api\Models
 */
class Agency extends Model implements Arrayable
{
    public $min_lines;
    public $max_lines;
    public $favorites;
    public $underdogs;
    public $odds;
    /**
     * @var AgencyLimits
     */
    public $limits;

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}