<?php

namespace Parley\Api\Models;

/**
 * Class Line
 * @package Parley\Api\Models
 */
class Line extends Model
{
    protected $id;
    protected $odds;
    protected $spread;
    protected $participant;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOdds()
    {
        return $this->odds;
    }

    /**
     * @return mixed
     */
    public function getSpread()
    {
        return $this->spread;
    }

    /**
     * @return mixed
     */
    public function getParticipant()
    {
        return $this->participant;
    }
}