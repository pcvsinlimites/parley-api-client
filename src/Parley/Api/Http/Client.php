<?php

namespace Parley\Api\Http;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Parley\Api\Http\Exceptions\Handler as ExceptionHandler;

/**
 * Class Client
 * @package Parley\Api\Http\Client
 */
class Client extends GuzzleClient
{
    /**
     * @var ExceptionHandler
     */
    protected $exceptionHandler;

    /**
     * Client constructor.
     * @param ExceptionHandler $exceptionHandler
     * @param array $config
     */
    public function __construct(ExceptionHandler $exceptionHandler, array $config = [])
    {
        $config['base_uri'] = env('PARLEY_API_URL', 'http://api.parley.com.ve/');
        $config['headers']['Accept'] = "application/json";
        $config['headers']['Accept-encoding'] = "gzip";

        parent::__construct($config);

        $this->exceptionHandler = $exceptionHandler;
    }

    /**
     * @param RequestInterface $request
     * @param array $options
     * @return mixed
     */
    public function perform(RequestInterface $request, array $options = [])
    {
        $options[RequestOptions::SYNCHRONOUS] = true;

        try {
            $response = $this->sendAsync($request, $options)->wait();

            if (method_exists($request, 'handle')) {
                return $request->handle($response);
            }
            else {
                return $response;
            }

        } catch (RequestException $exception) {
            $this->exceptionHandler->report($exception);

            return $this->exceptionHandler->handle($exception);
        }
    }

}