<?php

namespace Parley\Api\Http\Requests\Registration;

use GuzzleHttp\Psr7\Request;

/**
 * Class EnableUserForBetting
 * @package Parley\Api\Http\Requests\Registration
 */
class EnableUserForBetting extends Request
{
    /**
     * EnableUserForBetting constructor.
     * @param string $id
     */
    public function __construct($id)
    {
        parent::__construct('POST', 'user/enable/'.$id);
    }

}