<?php

namespace Parley\Api\Http\Requests\Registration;

use GuzzleHttp\Psr7\Request;
use Parley\Api\Http\Requests\User\UserResponse;

/**
 * Class RegisterUser
 * @package Parley\Api\Http\Requests\Registration
 */
class RegisterUser extends Request
{
    use UserResponse;

    /**
     * RegisterUser constructor.
     */
    public function __construct()
    {
        parent::__construct('POST', 'user/register');
    }

}