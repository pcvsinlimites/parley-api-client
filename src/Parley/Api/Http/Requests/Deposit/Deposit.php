<?php

namespace Parley\Api\Http\Requests\Deposit;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class Deposit extends Request {

    /**
     * Construct
     */
    public function __construct()
    {
        parent::__construct('POST', 'transfer/deposit');
    }

    /**
     * @param Response $response
     * @return mixed
     */
    public function handle(Response $response){

        $data = $response->getBody()->getContents();

        return json_decode($data);

    }

}