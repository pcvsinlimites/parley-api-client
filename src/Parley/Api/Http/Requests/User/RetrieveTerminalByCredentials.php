<?php

namespace Parley\Api\Http\Requests\User;

use App\User;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class RetrieveTerminalByCredentials
 * @package Parley\Api\Http\Requests\User
 */
class RetrieveTerminalByCredentials extends Request
{
    use UserResponse;

    /**
     * RetrieveTerminalByCredentials constructor.
     */
    public function __construct()
    {
        parent::__construct('POST', 'user/terminal/credentials');
    }
}