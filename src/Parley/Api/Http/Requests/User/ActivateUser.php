<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;

/**
 * Class ActivateUser
 * @package Parley\Api\Http\Requests\User
 */
class ActivateUser extends Request
{
    use UserResponse;

    /**
     * ActivateUser constructor.
     * @param string $email
     * @param \Psr\Http\Message\UriInterface|string $token
     */
    public function __construct($email, $token)
    {
        parent::__construct('POST', 'user/activate/'.$email.'/'.$token);
    }

}