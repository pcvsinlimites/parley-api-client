<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;

/**
 * Class EnableForBetting
 * @package Parley\Api\Http\Requests\User
 */
class EnableForBetting extends Request
{
    /**
     * EnableForBetting constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        parent::__construct('POST', 'user/enable/'.$id);
    }

}