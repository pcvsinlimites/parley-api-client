<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;

/**
 * Class RetrieveUserByCredentials
 * @package Parley\Api\Http\Requests\User
 */
class RetrieveUserByCredentials extends Request
{
    use UserResponse;

    /**
     * RetrieveUserByCredentials constructor.
     */
    public function __construct()
    {
        parent::__construct('POST', 'user/credentials');
    }
}