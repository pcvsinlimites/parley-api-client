<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class RetrieveUserMovements
 * @package Parley\Api\Http\Requests\User
 */
class RetrieveUserMovements extends Request
{
    /**
     * Construct
     */
    public function __construct()
    {
        parent::__construct('get', 'user/movements');
    }

    /**
     * Response to collection
     * @param Response $response
     * @return \Illuminate\Support\Collection
     */
    public function handle(Response $response){

        $data = $response->getBody()->getContents();

        return collect(json_decode($data));
    }
}