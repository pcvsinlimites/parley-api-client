<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class ValidateUserCredentials
 * @package Parley\Api\Http\Requests\User
 */
class ValidateUserCredentials extends Request
{
    /**
     * ValidateUserCredentials constructor.
     */
    public function __construct()
    {
        parent::__construct('POST', 'user/validate-credentials');
    }

    /**
     * @param Response $response
     * @return bool
     */
    public function handle(Response $response)
    {
        return $response->getBody()->getContents() === 'true';
    }
}