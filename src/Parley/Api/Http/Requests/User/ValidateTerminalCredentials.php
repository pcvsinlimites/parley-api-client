<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class ValidateTerminalCredentials
 * @package Parley\Api\Http\Requests\User
 */
class ValidateTerminalCredentials extends Request
{
    /**
     * ValidateTerminalCredentials constructor.
     */
    public function __construct()
    {
        parent::__construct('POST', 'user/terminal/validate-credentials');
    }

    /**
     * @param Response $response
     * @return bool
     */
    public function handle(Response $response)
    {
        return $response->getBody()->getContents() === 'true';
    }

}