<?php

namespace Parley\Api\Http\Requests\User;

use App\User;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class RetrieveTerminalById
 * @package Parley\Api\Http\Requests
 */
class RetrieveTerminalById extends Request
{
    use UserResponse;

    /**
     * RetrieveTerminalById constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        parent::__construct('GET', 'user/terminal/id/'.$id);
    }

}