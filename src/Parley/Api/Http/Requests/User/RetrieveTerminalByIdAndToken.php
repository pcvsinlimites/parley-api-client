<?php

namespace Parley\Api\Http\Requests\User;

use App\User;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class RetrieveTerminalByIdAndToken
 * @package Parley\Api\Http\Requests\User
 */
class RetrieveTerminalByIdAndToken extends Request
{
    use UserResponse;

    /**
     * RetrieveTerminalByIdAndToken constructor.
     * @param int $id
     * @param string $token
     */
    public function __construct($id, $token)
    {
        parent::__construct('GET', 'user/terminal/id-token/'.$id.'/'.$token);
    }

}