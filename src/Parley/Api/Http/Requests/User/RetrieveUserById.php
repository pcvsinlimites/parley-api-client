<?php

namespace Parley\Api\Http\Requests\User;

use App\User;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class RetrieveUserById
 * @package Parley\Api\Http\Requests
 */
class RetrieveUserById extends Request
{
    use UserResponse;

    /**
     * RetrieveUserById constructor.
     * @param string $id
     */
    public function __construct($id)
    {
        //Call parent request with specific parameters
        parent::__construct('GET', 'user/id/'.$id);
    }
}