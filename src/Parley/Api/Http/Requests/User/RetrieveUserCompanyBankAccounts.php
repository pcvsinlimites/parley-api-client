<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class RetrieveUserCompanyBankAccounts
 * @package Parley\Api\Http\Requests\User
 */
class RetrieveUserCompanyBankAccounts extends Request
{
    /**
     * @param string $id
     */
    public function __construct($id)
    {
        parent::__construct('GET', 'user/company-bank-accounts/'.$id);
    }

    /**
     * Response to collection
     * @param Response $response
     * @return \Illuminate\Support\Collection
     */
    public function handle(Response $response){

        $data = $response->getBody()->getContents();

        return collect(json_decode($data));

    }
}