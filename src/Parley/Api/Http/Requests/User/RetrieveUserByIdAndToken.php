<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;

/**
 * Class RetrieveUserByIdAndToken
 * @package Parley\Api\Http\Requests\User
 */
class RetrieveUserByIdAndToken extends Request
{
    use UserResponse;

    /**
     * RetrieveUserByIdAndToken constructor.
     * @param string $id
     * @param \Psr\Http\Message\UriInterface|string $token
     */
    public function __construct($id, $token)
    {
        parent::__construct('GET', 'user/id-token/'.$id.'/'.$token);
    }

}