<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Request;

/**
 * Class UpdateRememberToken
 * @package Parley\Api\Http\Requests\User
 */
class UpdateRememberToken extends Request
{
    /**
     * UpdateRememberToken constructor.
     * @param string $id
     */
    public function __construct($id)
    {
        parent::__construct('POST', 'user/update-remember-token/'.$id);
    }

}