<?php

namespace Parley\Api\Http\Requests\User;

use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Parley\Api\Models\Agency;
use Parley\Api\Models\AgencyLimits;
use Parley\Api\Models\CustomTerminal;
use Parley\Api\Models\Terminal;

/**
 * Class UserResponse
 * @package Parley\Api\Http\Requests\User
 */
trait UserResponse
{
    /**
     * Handle response
     * @param Response $response
     * @return Authenticatable
     */
    public function handle(Response $response)
    {
        $data = json_decode($response->getBody()->getContents(), true);

        if (!is_null($data)) {
            $class_name = config('parley-api.models.user');

            call_user_func([$class_name, 'unguard']);
            $user = new $class_name($data);
            call_user_func([$class_name, 'reguard']);

            if ($user->terminal) {
                $user->setAttribute('terminal', new Terminal($user->getAttribute('terminal')));

                if ($user->terminal->agency) {
                    $user->terminal->agency = new Agency($user->terminal->agency);

                    if ($user->terminal->agency->limits) {
                        $user->terminal->agency->limits = new AgencyLimits($user->terminal->agency->limits);
                    }
                }

                if ($user->terminal->custom) {
                    $user->terminal->custom = new CustomTerminal($user->terminal->custom);
                }
            }

            return $user;
        }
        else return null;
    }
}