<?php

namespace Parley\Api\Http\Requests\Betting;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Parley\Api\Models\BetType;
use Parley\Api\Models\Event;
use Parley\Api\Models\League;
use Parley\Api\Models\Line;
use Parley\Api\Models\Participant;
use Parley\Api\Models\Period;

/**
 * Class CurrentLines
 * @package Parley\Api\Http\Requests\Betting
 */
class CurrentLines extends Request
{
    /**
     * CurrentLines constructor.
     * @param string $format
     * @param int $sport_id
     */
    public function __construct($format, $sport_id = 0)
    {
        parent::__construct('GET', 'line/current/'.$format.'/'.$sport_id);
    }

    /**
     * Handle current lines response
     * @param Response $response
     * @return \Illuminate\Support\Collection
     */
    public function handle(Response $response)
    {
        $data = json_decode($response->getBody()->getContents(), true);
        $collection = collect();

        //dd($data[1]);

        foreach ($data[1] as $sport => $leagues) {

            foreach ($leagues as $league) {
                $events = collect();

                foreach ($league[1] as $event) {
                    $participants = collect();
                    $periods = collect();

                    foreach ($event[2] as $participant) {
                        $participants->push(new Participant([
                            'id' => $participant[1],
                            'rot' => $participant[0],
                            'name' => ($participant[1] > 5)
                                ? $participant[2] : trans('parley-api::main.participants.' . $participant[2])
                        ]));
                    }

                    foreach ($event[3] as $period) {
                        $m_period = new Period([
                            'id' => $period[0],
                            'name' => trans('parley-api::main.periods.' . $period[0])
                        ]);

                        foreach ($period[1] as $bet_type) {
                            $m_bet_type = new BetType([
                                'id' => $bet_type[0],
                                'name' => trans('parley-api::main.bet_types.' . $bet_type[0]),
                                'info' => $bet_type[2],
                            ]);

                            foreach ($bet_type[1] as $line) {
                                $m_bet_type->appendLine(new Line(([
                                    'id' => $line[0],
                                    'odds' => $line[2],
                                    'spread' => $line[1],
                                    'participant' => $line[3],
                                ])));
                            }

                            $m_period->appendBetType($m_bet_type);
                        }

                        $periods->push($m_period);
                    }

                    $events->push(new Event([
                        'id' => $event[0],
                        'startTime' => $event[1],
                        'participants' => $participants,
                        'periods' => $periods,
                    ]));
                }

                $collection->push(new League(['name' => static::league($league[0]), 'sport_id' => $sport, 'events' => $events]));
            }
        }

        $response = new \stdClass();
        $response->maxBTP = $data[0];
        $response->leagues = $collection;

        return $response;
    }

    /**
     * @param $str
     * @return mixed
     */
    protected static function league($str)
    {
        return \Lang::has('parley-api::leagues.'.$str) ?
            \Lang::get('parley-api::leagues.'.$str) :
            str_replace('-', ' ', $str);
    }

}