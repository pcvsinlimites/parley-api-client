<?php

namespace Parley\Api\Http\Requests\Betting;

use GuzzleHttp\Psr7\Request;

/**
 * Class StoreTicket
 * @package Parley\Api\Http\Requests\Betting
 */
class StoreTicket extends Request
{
    /**
     * StoreTicket constructor.
     */
    public function __construct()
    {
        parent::__construct('POST', 'ticket/store');
    }

}