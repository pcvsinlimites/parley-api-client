<?php

namespace Parley\Api\Http\Requests\Betting;

use GuzzleHttp\Psr7\Request;

/**
 * Class RetrieveBetTypeCombinations
 * @package Parley\Api\Http\Requests\Betting
 */
class RetrieveBetTypeCombinations extends Request
{
    /**
     * RetrieveBetTypeCombinations constructor.
     */
    public function __construct()
    {
        parent::__construct('GET', 'bet_type/combinations');
    }

}