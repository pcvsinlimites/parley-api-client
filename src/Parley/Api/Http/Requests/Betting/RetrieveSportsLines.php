<?php

namespace Parley\Api\Http\Requests\Betting;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Class RetrieveSportsLines
 * @package Parley\Api\Http\Requests\Betting
 */
class RetrieveSportsLines extends Request
{
    /**
     * Retrieve Sports from lines
     */
    public function __construct()
    {
        parent::__construct('GET', 'line/sport');
    }

    /**
     * Response to collection
     * @param Response $response
     * @return \Illuminate\Support\Collection
     */
    public function handle(Response $response){

        $data = $response->getBody()->getContents();

        return collect(json_decode($data));
    }

}