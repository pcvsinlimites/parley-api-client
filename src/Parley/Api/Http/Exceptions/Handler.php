<?php

namespace Parley\Api\Http\Exceptions;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Routing\Redirector;

/**
 * Class Handler
 * @package Parley\Api\Http\Exceptions
 */
class Handler implements \Parley\Api\Contracts\Exceptions\Handler
{
    /**
     * @param RequestException $exception
     * @return mixed
     */
    public function handle(RequestException $exception)
    {
        //Validation exception
        if ($exception->getResponse()->getStatusCode() === 422) {
            $errors = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return app(Redirector::class)->back()
                ->withInput()
                ->withErrors($errors);
        }

        throw $exception;
    }

    /**
     * @param RequestException $exception
     * @return mixed
     */
    public function report(RequestException $exception)
    {
    }
}