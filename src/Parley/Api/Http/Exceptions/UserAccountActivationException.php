<?php

namespace Parley\Api\Http\Exceptions;

/**
 * Class UserAccountActivationException
 * @package Parley\Api\Http\Exceptions
 */
class UserAccountActivationException extends \Exception
{

}