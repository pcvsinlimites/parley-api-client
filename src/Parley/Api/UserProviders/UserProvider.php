<?php

namespace Parley\Api\UserProviders;

use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Parley\Api\Http\Client;
use Parley\Api\Http\Requests\User\RetrieveUserByCredentials;
use Parley\Api\Http\Requests\User\RetrieveUserById;
use Parley\Api\Http\Requests\User\RetrieveUserByIdAndToken;
use Parley\Api\Http\Requests\User\UpdateRememberToken;
use Parley\Api\Http\Requests\User\ValidateUserCredentials;

/**
 * Class UserProvider
 * @package Parley\Api\UserProviders
 */
class UserProvider implements \Illuminate\Contracts\Auth\UserProvider
{
    /**
     * @var Client
     */
    private $client;

    /**
     * UserProvider constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return $this->client->perform(new RetrieveUserById($identifier));
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return $this->client->perform(new RetrieveUserByIdAndToken($identifier, $token));
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $this->client->perform(new UpdateRememberToken($user->getAuthIdentifier()), ['form_params' => compact('token')]);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        return $this->client->perform(new RetrieveUserByCredentials(), ['form_params' => $credentials]);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->client->perform(new ValidateUserCredentials(), ['form_params' => $credentials]);
    }
}