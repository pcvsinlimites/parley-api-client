<?php

namespace Parley\Api\UserProviders;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Parley\Api\Http\Client;
use Parley\Api\Http\Requests\User\RetrieveTerminalByCredentials;
use Parley\Api\Http\Requests\User\RetrieveTerminalById;
use Parley\Api\Http\Requests\User\RetrieveTerminalByIdAndToken;
use Parley\Api\Http\Requests\User\UpdateRememberToken;
use Parley\Api\Http\Requests\User\ValidateTerminalCredentials;

/**
 * Class TerminalProvider
 * @package Parley\Api\UserProviders
 */
class TerminalProvider implements UserProvider
{
    /**
     * @var Client
     */
    private $client;

    /**
     * TerminalProvider constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return $this->client->perform(new RetrieveTerminalById($identifier));
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return $this->client->perform(new RetrieveTerminalByIdAndToken($identifier, $token));
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $this->client->perform(new UpdateRememberToken($user->getAuthIdentifier()), ['form_params' => compact('token')]);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        return $this->client->perform(new RetrieveTerminalByCredentials(), ['form_params' => $credentials]);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->client->perform(new ValidateTerminalCredentials(), ['form_params' => $credentials]);
    }
}