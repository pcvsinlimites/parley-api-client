<?php

namespace Parley\Api\ViewComposers;

use Illuminate\Contracts\View\View;
use Parley\Api\Http\Client;
use Parley\Api\Http\Requests\Betting\CurrentLines;

/**
 * Class ClassicOddsInterface
 * @package Parley\Api\ViewComposers
 */
class ClassicOddsInterface
{
    /**
     * @var Client
     */
    private $api;

    /**
     * ClassicOddsInterface constructor.
     * @param Client $api
     */
    public function __construct(Client $api)
    {
        $this->api = $api;
    }

    /**
     * Bind data to view
     * @param View $view
     */
    public function compose(View $view)
    {
        $sport_id = $view->offsetExists('sport_id') ? $view->offsetGet('sport_id') : 0;

        $view->with('terminal', $this->api->perform(new CurrentLines(1, $sport_id)));
    }
}