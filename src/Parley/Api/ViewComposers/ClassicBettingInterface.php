<?php

namespace Parley\Api\ViewComposers;

use Illuminate\Contracts\View\View;
use Parley\Api\Http\Client;
use Parley\Api\Http\Requests\Betting\CurrentLines;

/**
 * Class ClassicBettingInterface
 * @package Parley\Api\ViewComposers
 */
class ClassicBettingInterface
{
    /**
     * @var Client
     */
    private $api;

    /**
     * ClassicBettingInterface constructor.
     * @param Client $api
     */
    public function __construct(Client $api)
    {
        $this->api = $api;
    }

    /**
     * Bind data to view
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('terminal', $this->api->perform(new CurrentLines(1)));
    }
}