<?php

namespace Parley\Api;

use Illuminate\Container\Container;
use Illuminate\Support\ServiceProvider;
use Parley\Api\Http\Client;
use Parley\Api\UserProviders\TerminalProvider;
use Parley\Api\UserProviders\UserProvider;
use Parley\Api\Http\Exceptions\Handler as ExceptionHandler;
use Parley\Api\Contracts\Exceptions\Handler as ExceptionHandlerInterface;
use Parley\Api\ViewComposers\ClassicBettingInterface;
use Parley\Api\ViewComposers\ClassicOddsInterface;

/**
 * Class ParleyApiServiceProvider
 * @package Parley\Api
 */
class ParleyApiServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Boot the service provider
     *
     * @return void
     */
    public function boot()
    {
        $this->registerClients();
        $this->extendAuthDrivers();
        $this->registerDirectives();

        $this->publish();
    }

    /**
     * Extend Auth drivers
     */
    protected function extendAuthDrivers()
    {
        $this->app['auth']->extend('parley-api:web', function ($app) {
            return new UserProvider($app[Client::class]);
        });
        
        $this->app['auth']->extend('parley-api:terminal', function ($app) {
            return new TerminalProvider($app[Client::class]);
        });
    }

    /**
     * Register API clients
     */
    protected function registerClients()
    {
    }

    /**
     * Register Blade directives
     */
    protected function registerDirectives()
    {
        \Blade::directive('api_error', function () {
            $label = '<div class="alert alert-danger"><?php echo trans("parley-api::main.exceptions.unknown") ?></div>';
            return "<?php if (\\Session::has('api_error')): ?> {$label} <?php endif; ?>";
        });
    }

    /**
     * Publish files
     */
    protected function publish()
    {
        $this->loadViewsFrom(__DIR__ . '/../../../resources/views', 'parley-api');

        $this->loadTranslationsFrom(__DIR__ . '/../../../resources/lang', 'parley-api');

        $this->publishes([
            __DIR__.'/../../../resources/lang' => base_path('resources/lang/parley-api')
        ], 'lang');
        
        $this->mergeConfigFrom(__DIR__ . '/../../../config/parley-api.php', 'parley-api');

        view()->composer(
            'parley-api::betting-interfaces.classic', ClassicBettingInterface::class
        );

        view()->composer(
            'parley-api::odds-interfaces.classic', ClassicOddsInterface::class
        );
    }
}