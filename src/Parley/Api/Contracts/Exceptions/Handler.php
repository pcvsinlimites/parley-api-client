<?php

namespace Parley\Api\Contracts\Exceptions;

use GuzzleHttp\Exception\RequestException;

/**
 * Interface Handler
 * @package Parley\Api\Contracts\Exceptions
 */
interface Handler
{
    /**
     * @param RequestException $exception
     * @return mixed
     */
    public function handle(RequestException $exception);

    /**
     * @param RequestException $exception
     * @return mixed
     */
    public function report(RequestException $exception);
}